
A vous de jouer...

-- 
Stéphane Soulet
06 32 57 22 28

20190702-brief-framework-js.md

Brief projet framework JS
=========================

# Brief projet
Ce projet est individuel et durera du 02/07/2019 au 26/07/2019. Faire une interface responsive avec REACT à partir d'une api REST tierce. Cette application devra comporter au moins :
* Manipulation du DOM virtuel
* Gestion de l'état des données (librairie redux)

# Explicitation
## DOM virtuel et gestion d'état
Exemple todo list : on récupère une liste de tâches, pour les conserver dans notre store. Si on modifie l'état d'une tâche (passage de todo en done), elle n'estt plus affichée dans la liste des tâches "todo". Dès qu'on a une modification d'un état, la vue (ou l'affichage) est mise à jour.

## Gestion des états:
Récupérer des données depuis l'api, de les traiter suivant différents états de l'application.

Exemple : on fetche la liste des restaurants (asiatiques, fast food, pizzerias), avec des checkboxs. Pouvoir filtrer un ou plusieurs types de restaurants sans refaire d'appel à l'api REST.

# Les étapes
## Installer REACT
## Choisir une API REST tierce
## Décrire le projet, faire une wireframe
## Implémenter le projet

# Objectifs
Utiliser un framework javascript (DOM virtuel, gestion d'états)
Utiliser une API REST tierce

Compétences visées:

* Web dynamique : transposer
* Maquettage : transposer

# Critères d'évaluation
* L'application est responsive
* L'application fonctionne, est réactive
* L'application ne fait pas des appels systématiques à l'api sur chaque interaction
* Le code source est architecturé

